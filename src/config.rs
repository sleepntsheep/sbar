static DEFAULT_CONFIG: &str = r#"
list:
  - name: exec
    prefix: "  "
      # item's prefix and suffix will overwrite global 
    params:
      - pamixer
      - "--get-volume-human"
    signal: 46
  - name: battery
    interval: 30
  - name: memory
    interval: 10
  - name: time
    params:
      - " %Y-%m-%d %I:%M:%S"
    interval: 1

# placed between every component
sep: "|"
# global prefix, suffix
prefix: " "
suffix: " "
"#;

use serde::Deserialize;
use serde_yaml;
use std::env::var;
use std::path::Path;
use std::fs::File;
use log::trace;

#[derive(Debug, Deserialize, PartialEq, Clone)]
pub struct Item {
    pub name: String,
    #[serde(default)]
    pub params: Vec<String>,
    #[serde(default)]
    pub signal: i32,
    #[serde(default)]
    pub interval: u64,
    #[serde(default)]
    pub str: Option<String>,
    #[serde(default)]
    pub prefix: String,
    #[serde(default)]
    pub suffix: String,
}

#[derive(Debug, PartialEq, Deserialize, Clone)]
pub struct Bar {
    pub list: Vec<Item>,
    pub sep: String,
    #[serde(default)]
    pub counter: u64,
    #[serde(default)]
    pub prefix: String,
    #[serde(default)]
    pub suffix: String,
    #[serde(default)]
    pub stdout: bool,
}

pub fn read_config(p: Option<String>) -> Bar {
    let path = match p {
        Some(p) => p,
        None => {
            let config_home = match var("XDG_CONFIG_HOME")
                .or_else(|_| var("HOME").map(|home| format!("{}/.config", home)))
                {
                    Ok(s) => s,
                    Err(_) => "~/.config".to_string(),
                };
            format!("{}/sbar/config.yaml", config_home)
        }
    };

    if Path::new(path.as_str()).exists() {
        trace!("Reading config from {}", path);
        let file = File::open(path).expect("Failed opening config file");
        serde_yaml::from_reader(file).expect("Failed parsing config")
    } else {
        trace!("Config at {} doesn't exists, defaulting", path);
        serde_yaml::from_str(DEFAULT_CONFIG).expect("Failed parsing default config")
    }
}

